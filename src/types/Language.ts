export type Language = {
	label: string,
	src: string,
	value: string,
}

export type Languages = {
	[name: string]: Language
}