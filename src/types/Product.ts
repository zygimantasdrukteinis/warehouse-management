export enum ProductTypes {
	Pharmacy,
	PetCare,
	Breakfast,
	Bakery,
	Meat,
	Seafood,
	Miscellaneous,
	Paper,
	Baby,
	CannedGoods,
	FrozenFoods,
}

export const ProductTypesObj = {
	0: 'Pharmacy',
	1: 'PetCare',
	2: 'Breakfast',
	3: 'Bakery',
	4: 'Meat',
	5: 'Seafood',
	6: 'Miscellaneous',
	7: 'Paper',
	8: 'Baby',
	9: 'CannedGoods',
	10: 'FrozenFoods',
}


export type ProductPriceHistory = {
	date: number,
	newPrice: number,
}

export type ProductQuantityHistory = {
	date: number,
	newQuantity: number,
}

export type ProductHistory = {
	priceHistory: ProductPriceHistory[],
	quantityHistory: ProductQuantityHistory[],
}

export type Product = {
	id: string,
	name: string,
	ean: string,
	type: ProductTypes,
	weight: number,
	color: string,
	active: boolean,
	quantity: number,
	price: number,
	history: ProductHistory,
}