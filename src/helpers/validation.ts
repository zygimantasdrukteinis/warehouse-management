import validbarcode from "barcode-validator";

export const validateProductProp = (prop: string, value: any) : boolean  => {
	let isValid = true;

	switch(prop) {
		case 'price':
		case 'weight':
			isValid = !isNaN(value) && value > 0;
			break;
		case 'quantity':
			isValid = !isNaN(value) && value >= 0 && value !== '';
			break;
		case 'color':
			isValid = /(?:#|0x)(?:[a-f0-9]{3}|[a-f0-9]{6})\b|(?:rgb|hsl)a?\([^)]*\)/.exec(value) !== null;
			break;
		case 'ean':
			isValid = validbarcode(value);
			break;
		case 'name':
		default:
			isValid = value !== '';
			break;
	}

	return isValid;
}
