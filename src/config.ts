export default {
	defaultLanguage: 'en',
	productQuantityHistoryDisplayLimit: 5,
	productPriceHistoryDisplayLimit: 5,
	messagesStackLimit: 3,
	// Configuration could also be added for:
	// * default currency;
	// * etc...
}