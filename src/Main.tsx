import React, { useContext} from 'react';
import { Switch, Route } from 'react-router-dom'
import Store  from './store/Store';
import Products from './containers/Products/Products';
import CreateProduct from './containers/CreateProduct/CreateProduct';
import EditProduct from './containers/EditProduct/EditProduct';
import ProductPreview from './containers/ProductPreview/ProductPreview';
import { makeStyles } from '@material-ui/core/styles';
import Backdrop from '@material-ui/core/Backdrop';
import CircularProgress from '@material-ui/core/CircularProgress';

type Props = {
	history: any,
};

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  container: {
    paddingTop: theme.spacing(12),
    paddingBottom: theme.spacing(8),
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
  backdrop: {
    zIndex: theme.zIndex.drawer + 1,
    color: '#fff',
  },
}));

export default function Main({ history } : Props) {
  const classes = useStyles();
  const { state } = useContext(Store);

  const renderRoutes = () : React.ReactElement => {
    return (
      <Switch>
        <Route exact path='/' render={() => <Products history={history} />}/>
        <Route exact path='/products/create' render={() => <CreateProduct history={history} />}/>
        <Route exact path='/products/:productId/edit' render={(routerProps) =>
          <EditProduct productId={routerProps.match.params.productId} history={history}/>}
        />
        <Route exact path='/products/:productId' render={(routerProps) =>
          <ProductPreview productId={routerProps.match.params.productId}/>}
        />
        <Route exact path='/products' render={() => <Products history={history}/>}/>
      </Switch>
    )
  }

  return (
    <React.Fragment>
      { state.loading ? (
        <Backdrop className={classes.backdrop} open>
          <CircularProgress color="inherit" />
        </Backdrop>
      ) : renderRoutes() }
    </React.Fragment>
  )
}
