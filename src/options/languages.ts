import EN from "../assets/flags/en.png";
import LT from "../assets/flags/lt.png";
import { Languages } from '../types/Language';

export default {
	"lt": {
		label: "LT",
		src: LT,
		value: "lt"
	},
	"en": {
		label: "EN",
		src: EN,
		value: "en"
	},
} as Languages;