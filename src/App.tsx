import React from 'react';
import { Router } from 'react-router-dom'
import { createBrowserHistory } from "history";
import { StoreProvider } from './store/Store';
import Main from './Main';
import config from './config';
import { SnackbarProvider } from 'notistack'
import Container from '@material-ui/core/Container';
import { makeStyles } from '@material-ui/core/styles';
import Navigation from './components/Navigation/Navigation';

const history = createBrowserHistory();

const useStyles = makeStyles((theme) => ({
  root: {
    display: 'flex',
  },
  container: {
    paddingTop: theme.spacing(12),
    paddingBottom: theme.spacing(8),
  },
  content: {
    flexGrow: 1,
    padding: theme.spacing(3),
  },
}));

function App() {
  const classes = useStyles();

  return (
    <Router history={history}>
      <StoreProvider>
        <SnackbarProvider maxSnack={config.messagesStackLimit}
          anchorOrigin={{
            vertical: 'top',
            horizontal: 'right',
          }}>
          <div className="App">
            <div className={classes.root}>
              <Navigation history={history}/>
              <Container className={classes.container} maxWidth="xl">
                <Main history={history}/>
              </Container>
            </div>
          </div>
        </SnackbarProvider>
      </StoreProvider>
    </Router>
  );
}

export default App;
