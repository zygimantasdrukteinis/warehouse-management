import { Product } from '../types/Product';

export const getProducts = () : Product[] => {
	const stringifiedProducts = localStorage.getItem('products');
	return stringifiedProducts ? JSON.parse(stringifiedProducts) : [];
}