import { Product, ProductTypes, ProductHistory } from '../types/Product';
import { v4 as uuidv4 } from 'uuid';

const createData = (
	id: string,
	name: string,
	ean: string,
	type: ProductTypes,
	weight: number,
	color: string,
	active: boolean,
	quantity: number,
	price: number,
	history: ProductHistory,
): Product => {
	return {
		id,
		name,
		ean,
		type,
		weight,
		color,
		active,
		quantity,
		price,
		history,
	};
}

const getInitialProductHistory = (): ProductHistory => ({
	priceHistory: [],
	quantityHistory: []
});

export const initProducts = () => {
	const currentProducts = JSON.parse(localStorage.getItem('products') || '[]');

	if (currentProducts.length) {
		// for demo purpose
		return;
	}

	const products = [
		createData(uuidv4(), 'Cupcake', '12424245', ProductTypes.Bakery, 1.5, '#fff', false, 0, 10.99, getInitialProductHistory()),
		createData(uuidv4(), 'Donut', '42424246', ProductTypes.Bakery, 0.1, '#fff', true, 1, 0.7, getInitialProductHistory()),
		createData(uuidv4(), 'Dog collar', '12424245', ProductTypes.PetCare, 0.1, '#fff', true, 1, 17.59, getInitialProductHistory()),
		createData(uuidv4(), 'Frozen yoghurt', '72424247', ProductTypes.FrozenFoods, 1.0, '#fff', true, 1, 2.59, getInitialProductHistory()),
		createData(uuidv4(), 'Gingerbread', '12424245', ProductTypes.Bakery, 1.0, '#fff', true, 1, 1.59, getInitialProductHistory()),
		createData(uuidv4(), 'Canned tuna', '72424247', ProductTypes.CannedGoods, 0.5, '#fff', true, 1, 2.59, getInitialProductHistory()),
		createData(uuidv4(), 'Ice cream sandwich', '42424246', ProductTypes.FrozenFoods, 1.5, '#fff', true, 1, 3.59, getInitialProductHistory()),
		createData(uuidv4(), 'Sea shells', '12424245', ProductTypes.Seafood, 2.4, '#fff', true, 1, 7.59, getInitialProductHistory()),
		createData(uuidv4(), 'Turkey breast', '42424246', ProductTypes.Meat, 1.5, '#fff', true, 1, 2.59, getInitialProductHistory()),
		createData(uuidv4(), 'Notebook', '72424247', ProductTypes.Paper, 0.2, '#fff', true, 1, 1.59, getInitialProductHistory()),
		createData(uuidv4(), 'Lobsters', '12424245', ProductTypes.Seafood, 1, '#fff', true, 1, 2.59, getInitialProductHistory()),
		createData(uuidv4(), 'Canned shrimps', '12424245', ProductTypes.CannedGoods, 0.5, '#fff', true, 1, 2.59, getInitialProductHistory()),
		createData(uuidv4(), 'Baby powder', '12424245', ProductTypes.Baby, 3.0, '#fff', true, 1, 10.59, getInitialProductHistory()),
	  ];

	  localStorage.setItem('products', JSON.stringify(products));
}