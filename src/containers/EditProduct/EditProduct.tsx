import React, { useReducer, useContext, useEffect } from 'react'
import { Product } from '../../types/Product';
import EditProductPage from '../../components/EditProduct/EditProductPage';
import { validateProductProp } from '../../helpers/validation';
import Store from '../../store/Store';
import moment from 'moment'
import { useTranslation } from 'react-i18next';
import { useSnackbar } from 'notistack';

type Props = {
	history: any,
	productId: string,
};

type State = {
	product: Product | null,
	loading: Boolean,
	redirecting: Boolean,
}

type Action =
	| { type: 'UPDATE_PRODUCT', payload: { prop: string, value: any, } }
	| { type: 'SET_PRODUCT', payload: { product: Product, } }
	| { type: 'SET_PROP_ERROR', payload: { errorsProp: string, value: boolean, } }
	| { type: 'SET_LOADING', payload: { loading: boolean }}

const initialState: State | any = {
	product: null,
	errors: {
		name: false,
		color: false,
		ean: false,
		quantity: false,
		price: false,
		weight: false,
	},
	loading: false,
};

const reducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case 'UPDATE_PRODUCT':
            return {
                ...state, product: {...state.product, [action.payload.prop]: action.payload.value},
			}
		case 'SET_PRODUCT': {
			return {
				...state, product: action.payload.product,
			}
		}
		case 'SET_PROP_ERROR':
			return {
				...state, errors: {...state.errors, [action.payload.errorsProp]: action.payload.value},
			}
		case 'SET_LOADING':
			return {
				...state, loading: action.payload.loading,
			}
        default:
            return state;
    }
}

export default function CreateProduct({ history, productId } : Props) {
	const { state, dispatch } = useContext(Store);
	const [ innerState, innerDispatch] = useReducer(reducer, initialState);
	const { t } = useTranslation();
	const { enqueueSnackbar } = useSnackbar();

	useEffect(() => {
		const product = state.products.find((product: Product) => product.id === productId);

		innerDispatch({
			type: 'SET_PRODUCT',
			payload: {
				product,
			},
		});
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

	const validatePropChange = (prop: string, value: any) => {
		let isValid = validateProductProp(prop, value);

		if (innerState.errors[prop] === !isValid) {
			return;
		}

		innerDispatch({
			type: 'SET_PROP_ERROR',
			payload: {
				errorsProp: prop,
				value: !isValid,
			}
		});
	}

	const handleChange = (event: React.ChangeEvent<{ name: string, value: any, valueAsNumber?: number,}>) => {
		const { name, value, valueAsNumber } = event.target;

		innerDispatch({
			type: 'UPDATE_PRODUCT',
			payload: {
				prop: name,
				value: valueAsNumber !== undefined && !isNaN(valueAsNumber) ? valueAsNumber : value,
			}
		});

		validatePropChange(name, value);
	};

	const handleSelect = (event: React.ChangeEvent<{ name: string, value: any }>): void | undefined => {
		const { name, value } = event.target;

		innerDispatch({
			type: 'UPDATE_PRODUCT',
			payload: {
				prop: name,
				value,
			},
		});
	}

	const handleColorChange = (color: string): void => {
		if (color === undefined) {
			return;
		}

		innerDispatch({
			type: 'UPDATE_PRODUCT',
			payload: {
				prop: 'color',
				value: color,
			},
		});

		let isValid = validateProductProp('color', color);

		if (innerState.errors['color'] === !isValid) {
			return;
		}

		innerDispatch({
			type: 'SET_PROP_ERROR',
			payload: {
				errorsProp: 'color',
				value: !isValid,
			}
		});
	}

	const handleToggle = (event: React.ChangeEvent<HTMLInputElement>): void => {
		const { name, checked } = event.target;

		innerDispatch({
			type: 'UPDATE_PRODUCT',
			payload: {
				prop: name,
				value: checked,
			},
		});
	}

	const isProductValid = () : boolean => {
		const productPropsErrors = Object.entries(innerState.product).map((value: any) => {
			const [ prop, propValue ] = [...value];
			let isValid = validateProductProp(prop, propValue);

			if (innerState.errors[prop] === !isValid) {
				return !isValid;
			}

			innerDispatch({
				type: 'SET_PROP_ERROR',
				payload: {
					errorsProp: prop,
					value: !isValid,
				}
			});

			return !isValid
		});

		return productPropsErrors.every(error => !error);
	}

	const updateProductHistory = () => {
		const oldProductData = state.products.find((product: Product) => product.id === productId);
		const quantityChanged = innerState.product.quantity !== oldProductData.quantity;
		const priceChanged = innerState.product.price !== oldProductData.price;

		if (quantityChanged) {
			innerState.product.history.quantityHistory.push({
				newQuantity: innerState.product.quantity,
				date: moment.utc(moment().format('YYYY-MM-DD HH:mm:ss')).valueOf(),
			});
		}

		if (priceChanged) {
			innerState.product.history.priceHistory.push({
				newPrice: innerState.product.price,
				date: moment.utc(moment().format('YYYY-MM-DD HH:mm:ss')).valueOf(),
			});
		}
	}

	const handleSubmit = () => {
		innerDispatch({
			type: 'SET_LOADING',
			payload: {
				loading: true,
			}
		});

		if (!isProductValid()) {
			enqueueSnackbar(t('editProduct.errorMessage'), { variant: 'error' });
			return innerDispatch({
				type: 'SET_LOADING',
				payload: {
					loading: false,
				}
			});
		}

		updateProductHistory();

		dispatch({
			type: 'UPDATE_PRODUCT',
			payload: {
				updatedProduct: innerState.product,
			},
		});

		innerDispatch({
			type: 'SET_LOADING',
			payload: {
				loading: false,
			}
		});

		enqueueSnackbar(t('editProduct.successMessage'), { variant: 'success' });
	}

    return (
        <React.Fragment>
			<EditProductPage
				product={innerState.product}
				errors={innerState.errors}
				loading={innerState.loading}
				handleChange={handleChange}
				handleSelect={handleSelect}
				handleColorChange={handleColorChange}
				handleToggle={handleToggle}
				handleSubmit={handleSubmit}
			/>
        </React.Fragment>
    )
}