import React, { useContext } from 'react'
import Store from '../../store/Store';
import { useTranslation } from 'react-i18next';
import ProductsPage from '../../components/Products/ProductsPage';
import { Product, ProductTypesObj } from '../../types/Product';
import CircularProgress from '@material-ui/core/CircularProgress';

type Props = {
	history: any,
};

export default function Products({ history } : Props) {

    const { state, dispatch } = useContext(Store);

    const { t } = useTranslation();

    const toggleProduct = (id: string) : void => {
        dispatch({
            type: 'TOGGLE_PRODUCT',
            payload: {
                id,
            },
        });
    }

    const deleteProduct = (id: string): void => {
        dispatch({
            type: 'DELETE_PRODUCT',
            payload: {
                id,
            },
        });
    }

    const openProductEdit = (rowData: any): void => {
        history.replace('')
        history.push(`/products/${rowData.id}/edit`)
    }

    const openProductPreview = (rowData: any): void => {
        history.replace('')
        history.push(`/products/${rowData.id}`)
    }

    const getTranslatedProducts = () : Product[] => {
        return state.products.map((product: Product) => {
            return {
                ...product,
                type: t(`product.types.${ProductTypesObj[product.type]}`),
            }
        })
    }

    return (
        <React.Fragment>
            {
                state.loading
                    ? (<CircularProgress/>)
                    : <ProductsPage
                        deleteProduct={deleteProduct}
                        toggleProduct={toggleProduct}
                        openProductEdit={openProductEdit}
                        openProductPreview={openProductPreview}
                        getTranslatedProducts={getTranslatedProducts}
                    />
            }
        </React.Fragment>
    )
}