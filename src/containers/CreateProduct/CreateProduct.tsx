import React, { useReducer, useContext } from 'react'
import { Product } from '../../types/Product';
import { v4 as uuidv4 } from 'uuid';
import CreateProductPage from '../../components/CreateProduct/CreateProductPage';
import { validateProductProp } from '../../helpers/validation';
import Store from '../../store/Store';
import { useTranslation } from 'react-i18next';
import { useSnackbar } from 'notistack';

type Props = {
	history: any,
};

type State = {
	product: Product | null,
	loading: Boolean,
	redirecting: Boolean,
}

type Action =
	| { type: 'UPDATE_PRODUCT', payload: { prop: string, value: any, } }
	| { type: 'RESET_PRODUCT' }
	| { type: 'SET_PROP_ERROR', payload: { errorsProp: string, value: boolean, } }
	| { type: 'SET_LOADING', payload: { loading: boolean }}

const generateInitialProduct = () : Product => (
	{
		id: uuidv4(),
		name: '',
		ean: '',
		type: 0,
		weight: 0,
		color: '#000',
		active: false,
		quantity: 1,
		price: 0,
		history: {
			quantityHistory: [],
			priceHistory: [],
		},
	}
);

const initialState: State | any = {
	product: generateInitialProduct(),
	errors: {
		name: false,
		color: false,
		ean: false,
		quantity: false,
		price: false,
		weight: false,
	},
	loading: false,
};

const reducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case 'UPDATE_PRODUCT':
            return {
                ...state, product: {...state.product, [action.payload.prop]: action.payload.value},
			}
		case 'SET_PROP_ERROR':
			return {
				...state, errors: {...state.errors, [action.payload.errorsProp]: action.payload.value},
			}
		case 'SET_LOADING':
			return {
				...state, redirecting: action.payload.loading,
			}
		case 'RESET_PRODUCT':
			return {
				...state, product: generateInitialProduct(), loading: false,
			}
        default:
            return state;
    }
}

export default function CreateProduct({ history } : Props) {
	const { dispatch } = useContext(Store);
	const [ innerState, innerDispatch] = useReducer(reducer, initialState);
	const { t } = useTranslation();
	const { enqueueSnackbar } = useSnackbar();

	const validatePropChange = (prop: string, value: any) => {
		let isValid = validateProductProp(prop, value);

		if (innerState.errors[prop] === !isValid) {
			return;
		}

		innerDispatch({
			type: 'SET_PROP_ERROR',
			payload: {
				errorsProp: prop,
				value: !isValid,
			}
		});
	}

	const handleChange = (event: React.ChangeEvent<{ name: string, value: any, valueAsNumber?: number, }>) => {
		const { name, value, valueAsNumber } = event.target;

		innerDispatch({
			type: 'UPDATE_PRODUCT',
			payload: {
				prop: name,
				value: valueAsNumber !== undefined && !isNaN(valueAsNumber) ? valueAsNumber : value,
			}
		});

		validatePropChange(name, value);
	};

	const handleSelect = (event: React.ChangeEvent<{ name: string, value: any }>): void | undefined => {
		const { name, value } = event.target;

		innerDispatch({
			type: 'UPDATE_PRODUCT',
			payload: {
				prop: name,
				value,
			},
		});
	}

	const handleColorChange = (color: string): void => {
		if (color === undefined) {
			return;
		}

		innerDispatch({
			type: 'UPDATE_PRODUCT',
			payload: {
				prop: 'color',
				value: color,
			},
		});

		let isValid = validateProductProp('color', color);

		if (innerState.errors['color'] === !isValid) {
			return;
		}

		innerDispatch({
			type: 'SET_PROP_ERROR',
			payload: {
				errorsProp: 'color',
				value: !isValid,
			}
		});
	}

	const handleToggle = (event: React.ChangeEvent<HTMLInputElement>): void => {
		const { name, checked } = event.target;

		innerDispatch({
			type: 'UPDATE_PRODUCT',
			payload: {
				prop: name,
				value: checked,
			},
		});
	}

	const handleSubmit = () => {
		innerDispatch({
			type: 'SET_LOADING',
			payload: {
				loading: true,
			}
		});

		const productPropsErrors = Object.entries(innerState.product).map((value: any) => {
			const [ prop, propValue ] = [...value];
			let isValid = validateProductProp(prop, propValue);

			if (innerState.errors[prop] === !isValid) {
				return !isValid;
			}

			innerDispatch({
				type: 'SET_PROP_ERROR',
				payload: {
					errorsProp: prop,
					value: !isValid,
				}
			});

			return !isValid
		});

		const allProductPropsValid = productPropsErrors.every(error => !error);

		if (!allProductPropsValid) {
			enqueueSnackbar(t('createProduct.errorMessage'), { variant: 'error' });
			return innerDispatch({
				type: 'SET_LOADING',
				payload: {
					loading: false,
				}
			});;
		}

		dispatch({
			type: 'ADD_PRODUCT',
			payload: {
				newProduct: innerState.product,
			},
		});

		enqueueSnackbar(t('createProduct.successMessage'), { variant: 'success' });

		innerDispatch({
			type: 'RESET_PRODUCT',
		});
	}

    return (
        <React.Fragment>
			<CreateProductPage
				product={innerState.product}
				errors={innerState.errors}
				loading={innerState.loading}
				handleChange={handleChange}
				handleSelect={handleSelect}
				handleColorChange={handleColorChange}
				handleToggle={handleToggle}
				handleSubmit={handleSubmit} />
        </React.Fragment>
    )
}