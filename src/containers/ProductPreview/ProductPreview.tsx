import React, { useEffect, useContext } from 'react'
import Store from '../../store/Store';
import ProductPreviewPage from '../../components/ProductPreview/ProductPreviewPage'

type Props = {
	productId: number,
}

export default function ProductPreview({ productId } : Props) {
	const { state, dispatch } = useContext(Store);

	useEffect(() => {
		dispatch({
			type: 'SELECT_PRODUCT',
			payload: {
				productId,
			},
		});
	// eslint-disable-next-line react-hooks/exhaustive-deps
	}, []);

    return (
        <React.Fragment>
			<ProductPreviewPage product={state.selectedProduct}/>
        </React.Fragment>
    )
}