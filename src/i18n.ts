import i18next from 'i18next';
import translationEN from './locales/en/translations.json';
import translationLT from './locales/lt/translations.json';
import { initReactI18next } from "react-i18next";
import config from './config';

i18next
	.use(initReactI18next)
	.init({
		interpolation: {
			// React already does escaping
			escapeValue: false,
		},
		lng: config.defaultLanguage,
		resources: {
			en: {
				translation: translationEN
			},
			lt: {
				translation: translationLT
			},
		},
	});

export default i18next