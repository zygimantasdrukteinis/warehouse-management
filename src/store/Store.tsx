import React, { useReducer, useEffect, createContext } from 'react';
import PropTypes from 'prop-types';
import { initProducts } from '../modules/initProducts';
import { getProducts } from '../modules/getProducts';
import { Product } from '../types/Product';

type StoreProps = {
    children: PropTypes.ReactNodeLike;
}

type Action =
    | { type: 'SET_PRODUCTS', payload: { products: Product[] } }
    | { type: 'UPDATE_PRODUCT', payload: { updatedProduct: Product, } }
    | { type: 'ADD_PRODUCT', payload: { newProduct: Product, } }
    | { type: 'DELETE_PRODUCT', payload: { id: string, } }
    | { type: 'TOGGLE_PRODUCT', payload: { id: string, } }
    | { type: 'SELECT_PRODUCT', payload: { productId: string, } }

type State = {
    products: Product[] | null,
    loading: Boolean,
}

const initialState: State | any = {
    products: null,
    selectedProduct: null,
    loading: true,
};

const Store = createContext(initialState);

const reducer = (state = initialState, action: Action) => {
    switch (action.type) {
        case 'SET_PRODUCTS':
            return {
                ...state, products: action.payload.products, loading: false,
            }
        case 'ADD_PRODUCT':
            return {
                ...state, products: [...state.products, action.payload.newProduct]
            }
        case 'UPDATE_PRODUCT': {
            const products = [...state.products];
            const index = products
                .findIndex((product:Product) => product.id === action.payload.updatedProduct.id);

            products[index] = action.payload.updatedProduct;

            return {
                ...state, products,
            }
        }
        case 'DELETE_PRODUCT': {
            const products = [...state.products];
            const deletedProductIndex = products.findIndex((product: Product) => product.id === action.payload.id);

            products.splice(deletedProductIndex, 1);

            return {
                ...state, products: products,
            }
        }
        case 'TOGGLE_PRODUCT': {
            const products = [...state.products];
            const toggledProductIndex = products
                .findIndex((product: Product) => product.id === action.payload.id);

            products[toggledProductIndex] = {
                ...products[toggledProductIndex],
                active: !products[toggledProductIndex].active
            };

            return {
                ...state, products: products,
            }
        }
        case 'SELECT_PRODUCT': {
            return {
                ...state, selectedProduct: state.products.find((product: Product) => product.id === action.payload.productId),
            }
        }
        default:
            return state;
    }
}

export const StoreProvider = (props: StoreProps) => {
	const [ state, dispatch] = useReducer(reducer, initialState);

    const populateProducts = async () => {
        const products = getProducts();

        dispatch({
            type: 'SET_PRODUCTS',
            payload: {
                products,
            },
        });
    }

    useEffect(() => {
        initProducts();
        populateProducts();
    }, []);

    useEffect(() => {
        localStorage.setItem('products', JSON.stringify(state.products))
    }, [state.products])

	return (
        <Store.Provider value={{state, dispatch}}>
          {props.children}
        </Store.Provider>
    )
}

export default Store