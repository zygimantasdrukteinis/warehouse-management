import React from 'react'
import Highcharts from 'highcharts'
import { Product, ProductPriceHistory } from '../../../types/Product'
import HighchartsReact from 'highcharts-react-official'
import config from '../../../config';
import { useTranslation } from 'react-i18next';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';

type Props = {
	product: Product
}

type PriceHistoryMap = {
	x: number,
	y: number,
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(1),
        padding: theme.spacing(1),
    },
  }));

export default function ProductPriceHistoryChart({ product }: Props) {
	const { t } = useTranslation();
	const classes = useStyles();

	const getProductPriceHistory = (): PriceHistoryMap[] => {
		const lastPriceHistory = [...product.history.priceHistory]
			.sort((historyX, historyY) => historyY.date - historyX.date)
			.slice(0, config.productPriceHistoryDisplayLimit);

		return lastPriceHistory.map((priceHistory: ProductPriceHistory) => {
			return {
				x: priceHistory.date,
				y: priceHistory.newPrice,
			};
		});
	};

	const options = {
		title: {
		  text: t('productPreview.priceHistory.chartTitle',
			  { limit: config.productPriceHistoryDisplayLimit }
			)
		},
		xAxis: {
			type: 'datetime',
			dateTimeLabelFormats: {
				millisecond: '%H:%M:%S',
				second: '%H:%M:%S',
				minute: '%H:%M',
				hour: '%H:%M',
				day: '%e. %b',
				week: '%e. %b',
				month: '%b \'%y',
				year: '%Y'
			},
			title: {
				text:  t('productPreview.priceHistory.xAxisTitle'),
			},
			tickPixelInterval: 200,
		},
		tooltip: {
			yDateFormat: '%Y-%m-%d: %H:%M:%S H',
			xDateFormat: '%Y-%m-%d: %H:%M:%S H',
			shared: true
		},
		yAxis: {
			title: {
				text: t('productPreview.priceHistory.yAxisTitle'),
			}
		},
		series: [{
			data: getProductPriceHistory(),
			name: t('productPreview.priceHistory.seriesName'),
		}],
	  }

	return (
		<Paper className={classes.paper} variant="outlined" square>
			<HighchartsReact
				highcharts={Highcharts}
				options={options}
			/>
		</Paper>
	)
}