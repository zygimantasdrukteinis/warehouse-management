import React from 'react'
import { Product, ProductTypesObj } from '../../../types/Product'
import { makeStyles, createStyles } from '@material-ui/core/styles';
import { useTranslation } from 'react-i18next';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Paper from '@material-ui/core/Paper';
import EuroIcon from '@material-ui/icons/Euro';
import Avatar from '@material-ui/core/Avatar';
import Divider from '@material-ui/core/Divider';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemText from '@material-ui/core/ListItemText';
import ListItemAvatar from '@material-ui/core/ListItemAvatar';
import VerticalSplitIcon from '@material-ui/icons/VerticalSplit';
import ViewModuleIcon from '@material-ui/icons/ViewModule';
import ColorLensIcon from '@material-ui/icons/ColorLens';

type Props = {
	product: Product,
}

const useStyles = makeStyles((theme) =>
  createStyles({
	paper: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(1),
		padding: theme.spacing(1),
    },
    root: {
		width: '100%',
		maxWidth: 560,
		backgroundColor: theme.palette.background.paper,
		margin: '0 auto'
	  },
	section1: {
		margin: theme.spacing(3, 2),
	},
  }),
);

export default function ProductDetails({ product }: Props) {
    const classes = useStyles();
	const { t } = useTranslation();

	const generateListItem = (icon: any, fieldName: string, fieldValue: string, divider: boolean = true) => {
		return (
			<React.Fragment>
				<ListItem>
					<ListItemAvatar>
						<Avatar>
							{icon}
						</Avatar>
					</ListItemAvatar>
					<ListItemText primary={
						<React.Fragment>
							<Typography component="span" variant="button" color="textPrimary">
								{fieldName}
							</Typography>
						</React.Fragment>
						}
						secondary={
							<React.Fragment>
								<Typography component="span" variant="body1" color="textPrimary">
									{fieldValue}
								</Typography>
							</React.Fragment>
						}
					/>
				</ListItem>
				{ divider && <Divider component="li" /> }
			</React.Fragment>
		)
	}

    return (
        <React.Fragment>
				<div className={classes.root}>
					<Paper className={classes.paper} variant="outlined" square>
						<div className={classes.section1}>
							<Grid container alignItems="center">
								<Grid item xs={12}>
									<Typography gutterBottom variant="h4">
										{product.name}
									</Typography>
								</Grid>
							</Grid>
							<List className={classes.root}>
								{ generateListItem(<EuroIcon />, t('product.price'), `$${product.price}`) }
								{ generateListItem(<ViewModuleIcon />, t('product.quantity'), `${product.quantity}`) }
								{ generateListItem('W', t('product.weight'), t('product.weightValue', { weight: product.weight })) }
								{ generateListItem(<VerticalSplitIcon />, t('product.ean'), `${product.ean}`) }
								{ generateListItem('T', t('product.type'), t(`product.types.${ProductTypesObj[product.type]}`)) }
								{ generateListItem(<ColorLensIcon/>, t('product.color'), product.color, false) }
							</List>
						</div>
					</Paper>
				</div>
        </React.Fragment>
    )
}