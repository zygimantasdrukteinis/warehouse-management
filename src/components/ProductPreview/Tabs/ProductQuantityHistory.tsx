import React from 'react'
import Highcharts from 'highcharts'
import { Product, ProductQuantityHistory } from '../../../types/Product'
import { useTranslation } from 'react-i18next';
import HighchartsReact from 'highcharts-react-official'
import config from '../../../config';
import Paper from '@material-ui/core/Paper';
import { makeStyles } from '@material-ui/core/styles';

type Props = {
	product: Product
}

type QuantityHistoryMap = {
	x: number,
	y: number,
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(1),
        padding: theme.spacing(1),
    },
  }));

export default function ProductQuantityHistoryChart({ product }: Props) {
	const { t } = useTranslation();
	const classes = useStyles();

	const getQuantityHistory = (): QuantityHistoryMap[] => {
		const lastQuantityHistory = [...product.history.quantityHistory]
			.sort((historyX, historyY) => historyY.date - historyX.date)
			.slice(0, config.productQuantityHistoryDisplayLimit);

		return lastQuantityHistory.map((quantityHistory: ProductQuantityHistory) => {
			return {
				x: quantityHistory.date,
				y: quantityHistory.newQuantity,
			};
		});
	};

	const options = {
		title: {
		  text: t('productPreview.quantityHistory.chartTitle',
		  		{ limit: config.productQuantityHistoryDisplayLimit }
			)
		},
		xAxis: {
			type: 'datetime',
			dateTimeLabelFormats: {
				millisecond: '%H:%M:%S',
				second: '%H:%M:%S',
				minute: '%H:%M',
				hour: '%H:%M',
				day: '%e. %b',
				week: '%e. %b',
				month: '%b \'%y',
				year: '%Y'
			},
			title: {
				text: t('productPreview.quantityHistory.xAxisTitle')
			},
			tickPixelInterval: 200,
		},
		tooltip: {
			yDateFormat: '%Y-%m-%d: %H:%M:%S H',
			xDateFormat: '%Y-%m-%d: %H:%M:%S H',
			shared: true
		},
		yAxis: {
			title: {
				text: t('productPreview.quantityHistory.yAxisTitle')
			}
		},
		series: [{
			data: getQuantityHistory(),
			name: t('productPreview.quantityHistory.seriesName'),
		}],
	  }

	return (
		<Paper className={classes.paper} variant="outlined" square>
			<HighchartsReact
				highcharts={Highcharts}
				options={options}
			/>
		</Paper>
	)
}