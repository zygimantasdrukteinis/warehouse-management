import React from 'react'
import { Product } from '../../types/Product'
import ProductDetails from './Tabs/ProductDetails';
import ProductPriceHistory from './Tabs/ProductPriceHistory';
import ProductQuantityHistory from './Tabs/ProductQuantityHistory';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Container from '@material-ui/core/Container';
import CircularProgress from '@material-ui/core/CircularProgress';

type Props = {
	product: Product;
}

type TabPanelProps = {
    children ? : React.ReactNode;
    index: any;
    value: any;
}

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    progress: {
        marginTop: theme.spacing(2),
    },
}));

export default function ProductPreviewPage({ product }: Props) {
    const classes = useStyles();
    const [value, setValue] = React.useState(0);
    const { t } = useTranslation();

    const handleChange = (event: React.ChangeEvent<{}>, newValue: number) => {
        setValue(newValue);
    };

    const displayProductPreview = () => {
        if (!product) {
            return <CircularProgress className={classes.progress}/>;
        }

        return (
            <React.Fragment>
                <Tabs
                    value={value}
                    onChange={handleChange}
                    indicatorColor="primary"
                    textColor="primary"
                    centered
                >
                    <Tab label={t('productPreview.productDetails.title')} />
                    <Tab label={t('productPreview.priceHistory.title')} />
                    <Tab label={t('productPreview.quantityHistory.title')} />
                </Tabs>
                <TabPanel value={value} index={0}>
                    <ProductDetails product={product}/>
                </TabPanel>
                <TabPanel value={value} index={1}>
                    <ProductPriceHistory product={product}/>
                </TabPanel>
                <TabPanel value={value} index={2}>
                    <ProductQuantityHistory product={product}/>
                </TabPanel>
            </React.Fragment>
        )
    }

    function TabPanel(props: TabPanelProps) {
        const { children, value, index, ...other } = props;

        return (
          <div
            role="tabpanel"
            hidden={value !== index}
            id={`product-tab-${index}`}
            aria-labelledby={`product-tab-${index}`}
            {...other}
          >
            {value === index &&
                children
            }
          </div>
        );
    }

    return (
        <React.Fragment>
            <Container maxWidth="md">
                <Paper className={classes.paper}>
                   {displayProductPreview()}
                </Paper>
            </Container>
        </React.Fragment>
    )
}