import React from 'react'
import { useTranslation } from 'react-i18next';
import { Product } from '../../types/Product';
import { Column } from '../../types/Column';
import MaterialTable from 'material-table';
import Checkbox from '@material-ui/core/Checkbox';

type Props = {
    deleteProduct(id: string): void;
    toggleProduct(id: string): void;
    openProductEdit(rowData: any): void,
    openProductPreview(rowData: any): void,
    getTranslatedProducts(): Product[],
}

export default function ProductsPage({
    deleteProduct,
    toggleProduct,
    openProductEdit,
    openProductPreview,
    getTranslatedProducts,
}: Props) {

    const { t } = useTranslation();

    const columns: Column[] = [{
            title: t('product.name'),
            field: 'name'
        },
        {
            title: t('product.ean'),
            field: 'ean'
        },
        {
            title: t('product.type'),
            field: 'type',
        },
        {
            title: t('product.weightKgLabel'),
            field: 'weight',
            type: 'numeric',
        },
        {
            title: t('product.color'),
            field: 'color',
        },
        {
            title: t('product.quantity'),
            field: 'quantity',
            type: 'numeric',
        },
        {
            title: t('product.priceEurLabel'),
            field: 'price',
            type: 'numeric',
        },
        {
            title: t('product.active'),
            field: 'active',
            type: 'boolean',
            render: rowData => <Checkbox
            checked={rowData.active}
            onChange={() => {toggleProduct(rowData.id)}}
            inputProps={{ 'aria-label': 'primary checkbox' }}
        />
        },
    ]

    return (
        <React.Fragment>
             <MaterialTable
                title={t('productsTable.title')}
                columns={columns}
                data={getTranslatedProducts()}
                editable={{
                    onRowDelete: (rawData) =>
                        new Promise((resolve) => {
                            setTimeout(() => {
                            resolve();
                            deleteProduct(rawData.id);
                            }, 600);
                        }),
                }}
                localization={{
                    header: {
                        actions: t('productsTable.actions.title')
                    },
                    body: {
                        editRow: {
                            deleteText: t('productsTable.actions.deleteConfirmationText'),
                            cancelTooltip: t('productsTable.actions.tooltips.cancel'),
                            saveTooltip: t('productsTable.actions.tooltips.save'),
                        },
                        emptyDataSourceMessage: t('productsTable.emptyDataSourceMessage'),
                    },
                    pagination: {
                        labelRowsSelect: t('productsTable.pagination.labelRowsSelect'),
                        firstAriaLabel: t('productsTable.pagination.firstAriaLabel'),
                        firstTooltip: t('productsTable.pagination.tooltips.first'),
                        previousAriaLabel: t('productsTable.pagination.previousAriaLabel'),
                        previousTooltip: t('productsTable.pagination.tooltips.previous'),
                        nextAriaLabel: t('productsTable.pagination.nextAriaLabel'),
                        nextTooltip: t('productsTable.pagination.tooltips.next'),
                        lastAriaLabel: t('productsTable.pagination.lastAriaLabel'),
                        lastTooltip: t('productsTable.pagination.tooltips.last'),
                    },
                    toolbar: {
                        searchTooltip: t('productsTable.toolbar.tooltips.search'),
                        searchPlaceholder: t('productsTable.toolbar.searchPlaceholder'),
                    },
                }}
                actions={[
                    {
                        icon: 'launch',
                        tooltip:  t('productsTable.actions.tooltips.open'),
                        onClick: (event, rowData) => openProductPreview(rowData),
                    },
                    {
                        icon: 'edit',
                        tooltip: t('productsTable.actions.tooltips.edit'),
                        onClick: (event, rowData) => openProductEdit(rowData),
                    },
                  ]}
                options={{
                    actionsColumnIndex: -1,
                }}
            />
        </React.Fragment>
    )
}