import React from 'react'
import { Product, ProductTypesObj } from '../../types/Product';
import { useTranslation } from 'react-i18next';
import { makeStyles } from '@material-ui/core/styles';
import FormControl from '@material-ui/core/FormControl';
import TextField from '@material-ui/core/TextField';
import InputLabel from '@material-ui/core/InputLabel';
import Grid from '@material-ui/core/Grid';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import InputAdornment from '@material-ui/core/InputAdornment';
import ColorPicker from "material-ui-color-picker";
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Switch from '@material-ui/core/Switch'

type Props = {
    product: Product,
    errors: {
        name: boolean,
		color: boolean,
		ean: boolean,
		quantity: boolean,
		price: boolean,
		weight: boolean,
    },
    handleChange: React.ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>,
    handleSelect(event: React.ChangeEvent<{ value: unknown }>): void | undefined,
    handleColorChange(color: string): void,
    handleToggle(event: React.ChangeEvent<HTMLInputElement>): void,
};

const useStyles = makeStyles((theme) => ({
    form: {
        paddingTop: theme.spacing(6),
        paddingBottom: theme.spacing(2),
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
    },
    colorPicker: {
        color: 'inherit',
    },
  }));

export default function ProductForm({
    product,
    errors,
    handleChange,
    handleSelect,
    handleColorChange,
    handleToggle,
}: Props) {
    const classes = useStyles();

	const { t } = useTranslation();

    const typeSelect = () => {
        return (
            <Select
                name="type"
                id="type"
                value={product.type}
                onChange={handleSelect}
            >
                {Object.values(ProductTypesObj).map((value, index) =>
                    <MenuItem key={value} value={index}>{t(`product.types.${value}`)}</MenuItem>
                )}
            </Select>
        )
    }

    return (
        <React.Fragment>
			<form className={classes.form} noValidate autoComplete="off">
				<Grid container spacing={3}>
					<Grid item xs={12} sm={6} lg={3}>
						<FormControl error={errors.name} fullWidth>
							<TextField
								error={errors.name}
								{...(errors.name && { helperText: t('productFormErrors.name') })}
								name="name"
								id="name"
								label={t('product.name')}
								value={product.name}
								onChange={handleChange}
							/>
						</FormControl>
					</Grid>
					<Grid item xs={12} sm={6} lg={3}>
						<FormControl fullWidth>
							<TextField
								error={errors.ean}
								{...(errors.ean && { helperText: t('productFormErrors.ean') })}
								label={t('product.ean')}
								name="ean"
								id="ean"
								value={product.ean}
								onChange={handleChange}
							/>
						</FormControl>
					</Grid>
					<Grid item xs={12} sm={6} lg={3}>
						<FormControl fullWidth>
							<InputLabel htmlFor="type">{t('product.type')}</InputLabel>
							{typeSelect()}
						</FormControl>
					</Grid>
					<Grid item xs={12} sm={6} lg={3}>
						<FormControl fullWidth>
							<TextField
								error={errors.weight}
								{...(errors.weight && { helperText: t('productFormErrors.weight') })}
								label={t('product.weight')}
								id="weight"
								InputProps={{
									startAdornment: <InputAdornment position="start">Kg</InputAdornment>,
								}}
								type="number"
								name="weight"
								value={product.weight}
								onChange={handleChange}
							/>
						</FormControl>
					</Grid>
					<Grid item xs={12} sm={6} lg={3}>
						<FormControl error={errors.color} fullWidth>
							<ColorPicker
								error={errors.color}
								id="color"
								name="color"
								label={t('product.color')}
								onChange={handleColorChange}
								TextFieldProps={{ value: product.color, ...(errors.color && { helperText: t('productFormErrors.color') }) }}
								InputLabelProps={{shrink:true}}
								InputProps={{className: classes.colorPicker}}
							/>
						</FormControl>
					</Grid>
					<Grid item xs={12} sm={6} lg={3}>
						<FormControl fullWidth>
							<TextField
								error={errors.quantity}
								{...(errors.quantity && { helperText: t('productFormErrors.quantity') })}
								type="number"
								name="quantity"
								label={t('product.quantity')}
								id="component-simple"
								value={product.quantity}
								onChange={handleChange}
							/>
						</FormControl>
					</Grid>
					<Grid item xs={12} sm={6} lg={3}>
						<FormControl error={errors.price} fullWidth>
							<TextField
								error={errors.price}
								{...(errors.price && { helperText: t('productFormErrors.price') })}
								type="number"
								id="price"
								name="price"
								label={t('product.price')}
								value={product.price}
								onChange={handleChange}
								InputProps={{
									startAdornment: <InputAdornment position="start">€</InputAdornment>,
								}}
							/>
						</FormControl>
					</Grid>
					<Grid item xs={12} sm={12} lg={12}>
						<FormControl error={errors.price} fullWidth>
							<FormControlLabel
								control={<Switch checked={product.active} onChange={handleToggle} name="active" />}
								label={t('product.active')}
							/>
						</FormControl>
					</Grid>
				</Grid>
			</form>
        </React.Fragment>
    )
}