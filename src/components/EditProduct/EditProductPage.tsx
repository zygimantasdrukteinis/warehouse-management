import React from 'react'
import { Product } from '../../types/Product';
import ProductFrom from '../Shared/ProductForm';
import { makeStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Container from '@material-ui/core/Container';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';
import CircularProgress from '@material-ui/core/CircularProgress';
import { useTranslation } from 'react-i18next';

type Props = {
    product: Product,
    errors: {
        name: boolean,
		color: boolean,
		ean: boolean,
		quantity: boolean,
		price: boolean,
		weight: boolean,
    },
    loading: boolean,
    handleChange: React.ChangeEventHandler<HTMLTextAreaElement | HTMLInputElement>,
    handleSelect(event: React.ChangeEvent<{ value: unknown }>): void | undefined,
    handleColorChange(color: string): void,
    handleToggle(event: React.ChangeEvent<HTMLInputElement>): void,
    handleSubmit(): void,
};

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(3),
        marginBottom: theme.spacing(3),
        padding: theme.spacing(2),
        [theme.breakpoints.up(600 + theme.spacing(3) * 2)]: {
            marginTop: theme.spacing(6),
            marginBottom: theme.spacing(6),
            padding: theme.spacing(3),
        },
    },
    buttons: {
        display: 'flex',
        justifyContent: 'flex-end',
    },
    button: {
        marginTop: theme.spacing(3),
        marginLeft: theme.spacing(1),
    },
    progress: {
        marginTop: theme.spacing(2),
    },
  }));

export default function EditProductPage({
    product,
    errors,
    loading,
    handleChange,
    handleSelect,
    handleColorChange,
    handleToggle,
    handleSubmit,
}: Props) {

    const classes = useStyles();
    const { t } = useTranslation();

    const displayProduct = () => {
        if (!product) {
            return <CircularProgress className={classes.progress}/>;
        }

        return (
            <Paper className={classes.paper}>
                <Typography component="h1" variant="h4" align="center">
                    {t('editProduct.title')}
                </Typography>
                <ProductFrom
                    product={product}
                    errors={errors}
                    handleChange={handleChange}
                    handleSelect={handleSelect}
                    handleColorChange={handleColorChange}
                    handleToggle={handleToggle}
                />
                <div className={classes.buttons}>
                    <Button disabled={loading} className={classes.button}
                        variant="contained"
                        color="primary"
                        onClick={handleSubmit}
                    >
                        {t('editProduct.save')}
                    </Button>
                </div>
            </Paper>
        );
    }

    return (
        <React.Fragment>
            <Container maxWidth="md">
                {displayProduct()}
            </Container>
        </React.Fragment>
    )
}