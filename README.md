## Warehouse Management Demo

This is a demo project for warehouse management.

### Architecture
For architecture - container pattern was used.

### Configuration
Minimal configuration was added, config example:

    export default {
    	defaultLanguage: 'en',
    	productQuantityHistoryDisplayLimit: 5,
    	productPriceHistoryDisplayLimit: 5,
    	messagesStackLimit: 3,
    }

In future, more configuration could be added:

- Currencies;
- Metrics;
- Logging (**loggly** for ex);

### Supported EAN Types
- EAN-13;
- EAN-8;
- UPC-A;
- UPC-E;

### React Scripts

### `npm run start`

Runs the app in the development mode.

### `npm test`

Launches the test runner in the interactive watch mode.

### `npm run build`

Builds the app for production to the `build` folder.
